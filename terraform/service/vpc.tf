##################################################
### vpc definition
##################################################

resource "aws_vpc" "assignment" {
  cidr_block = "172.23.0.0/20"
  tags = {
    Name        = "assignment-vpc"
    Environment = "assignment"
  }
}

##################################################
### internet gateways
##################################################
resource "aws_internet_gateway" "assignment-igw" {
  vpc_id = aws_vpc.assignment.id

  tags = {
    Name        = "assignment-igw"
    Environment = "assignment"
  }
}

##################################################
### subnets 
##################################################

### Public Subnets
resource "aws_subnet" "public-subnet-a" {
  vpc_id            = aws_vpc.assignment.id
  cidr_block        = "172.23.0.0/23"
  availability_zone = "eu-central-1a"
  map_public_ip_on_launch = true

  tags = {
    Name        = "assignment-public-subnet-a"
    Environment = "assignment"
  }
}

resource "aws_subnet" "public-subnet-b" {
  vpc_id            = aws_vpc.assignment.id
  cidr_block        = "172.23.4.0/23"
  availability_zone = "eu-central-1b"
  map_public_ip_on_launch = true

  tags = {
    Name        = "assignment-public-subnet-b"
    Environment = "assignment"
  }
}

resource "aws_subnet" "public-subnet-c" {
  vpc_id            = aws_vpc.assignment.id
  cidr_block        = "172.23.8.0/23"
  availability_zone = "eu-central-1c"
  map_public_ip_on_launch = true

  tags = {
    Name        = "assignment-public-subnet-c"
    Environment = "assignment"
  }
}

##################################################
### route tables
##################################################
resource "aws_route_table" "assignment-public-routetable" {
  vpc_id = aws_vpc.assignment.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.assignment-igw.id
  }

  tags = {
    Name        = "assignment-public-routetable"
    Environment = "assignment"
  }
}

resource "aws_route_table_association" "assignment-public-routetable-assoc-a" {
  subnet_id      = aws_subnet.public-subnet-a.id
  route_table_id = aws_route_table.assignment-public-routetable.id
}

resource "aws_route_table_association" "assignment-public-routetable-assoc-b" {
  subnet_id      = aws_subnet.public-subnet-b.id
  route_table_id = aws_route_table.assignment-public-routetable.id
}

resource "aws_route_table_association" "assignment-public-routetable-assoc-c" {
  subnet_id      = aws_subnet.public-subnet-c.id
  route_table_id = aws_route_table.assignment-public-routetable.id
}

output "vpc_id" {
  value = aws_vpc.assignment.id
}

output "pub-subnet-a" {
  value = aws_subnet.public-subnet-a.id
}

output "pub-subnet-b" {
  value = aws_subnet.public-subnet-b.id
}

output "pub-subnet-c" {
  value = aws_subnet.public-subnet-c.id
}

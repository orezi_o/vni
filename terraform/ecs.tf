
module "assignment" {
  source = "./service"
}
##################################################
### Ecs Cluster definition
#
#### this the cluster name
resource "aws_ecs_cluster" "assignment-cluster" {
  name = "assignment-cluster"
}

#### this defines the asg
resource "aws_autoscaling_group" "assignment-asg" {
  name                      = "assignment-asg"
  max_size                  = var.asg_max_size
  min_size                  = var.asg_min_size
  health_check_grace_period = 300
  health_check_type         = "EC2"
  desired_capacity          = var.asg_desired_capacity

  launch_configuration      = aws_launch_configuration.assignment-launch-config.name
  vpc_zone_identifier       = [module.assignment.pub-subnet-a, module.assignment.pub-subnet-c, module.assignment.pub-subnet-c]
  termination_policies      = ["OldestLaunchConfiguration"]
  
  enabled_metrics = [
    "GroupDesiredCapacity",
    "GroupInServiceInstances",
    "GroupMaxSize",
    "GroupMinSize",
    "GroupPendingInstances",
    "GroupStandbyInstances",
    "GroupTerminatingInstances",
    "GroupTotalInstances",
  ]

  lifecycle {
    create_before_destroy = true
    ignore_changes        = [desired_capacity]
  }

  tags = [
    {
      key                 = "Name"
      value               = "assignment-ecs-instance"
      propagate_at_launch = true
    },
    {
      key                 = "Environment"
      value               = "assignment"
      propagate_at_launch = true
    },
  ]
}

resource "aws_autoscaling_policy" "assignment-ecs-cluster-scaleup" {
  name                   = "assignment-ecs-cluster-scaleup"
  scaling_adjustment     = 2
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 300
  autoscaling_group_name = aws_autoscaling_group.assignment-asg.name
}

resource "aws_autoscaling_policy" "assignment-ecs-cluster-scaledown" {
  name                   = "assignment-ecs-cluster-scaledown"
  scaling_adjustment     = -1
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 300
  autoscaling_group_name = aws_autoscaling_group.assignment-asg.name
}

#### this defines the launch config for the ecs cluster
resource "aws_launch_configuration" "assignment-launch-config" {
  name_prefix = "assignment-launch-config-"
  image_id    = data.aws_ami.latest_ecs.id
  lifecycle {
    create_before_destroy = true
    ignore_changes        = [user_data]
  }
  instance_type        = var.instance_type
  iam_instance_profile = aws_iam_instance_profile.assignment-ecs-instance-profile.name
  key_name             = "assignment-key-pair"
  security_groups      = [aws_security_group.assignment-memberonly-access-sg.id, aws_security_group.assignment-remote-access-sg.id]
  root_block_device {
    volume_type = "standard"
    volume_size = var.instance_volume_size
  }
  #### this installs ssm agent on startup and sets the ecs cluster in ecs config
  user_data = file("install_ssm_agent.sh")
}

resource "aws_security_group" "assignment-remote-access-sg" {
  name        = "assignment-remote-access-sg"
  description = "Allow ssh from public subnet"
  vpc_id      = module.assignment.vpc_id

  tags = {
    Name        = "assignment-remote-access-sg"
    Environment = "assignment"
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["172.23.0.0/23", "172.23.8.0/23", "172.23.4.0/23"]
    description = "allow ssh from public subnet"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    description = "allow all"
  }
}

### Instance role
resource "aws_iam_role" "assignment-ecs-instance-role" {
  name               = "assignment-ecs-instance-role"
  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
    {
        "Action": "sts:AssumeRole",
        "Principal": {
        "Service": "ec2.amazonaws.com"
        },
        "Effect": "Allow"
    }
    ]
}
EOF
}

## attach policy to role
resource "aws_iam_role_policy_attachment" "assignment-ecs-instance-role-attach" {
  role       = aws_iam_role.assignment-ecs-instance-role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceforEC2Role"
}

## allow instance use session manager
resource "aws_iam_role_policy_attachment" "assignment-ecs-instance-role-attach-session-manager" {
  role       = aws_iam_role.assignment-ecs-instance-role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
}

### apply the policy to the IAM role
resource "aws_iam_instance_profile" "assignment-ecs-instance-profile" {
  name = "assignment-ecs-instance-profile"
  role = aws_iam_role.assignment-ecs-instance-role.name
}

data "aws_ami" "latest_ecs" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "owner-alias"
    values = ["amazon"]
  }

  filter {
    name   = "name"
    values = ["*amazon-ecs-optimized"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

resource "aws_iam_role" "task-execution-role" {
  name               = "ecsTaskExecutionRole"
  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
    {
        "Action": "sts:AssumeRole",
        "Principal": {
        "Service": "ecs-tasks.amazonaws.com"
        },
        "Effect": "Allow"
    }
    ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "attach-task-execution-policy" {
  role       = aws_iam_role.task-execution-role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceforEC2Role"
}

resource "aws_ecs_task_definition" "task-definition" {
  family                = var.service_name
  container_definitions = templatefile("task-definitions/vni-task-definition.json.tmpl", { "memory" = var.task_memory_limit})
  
  execution_role_arn    = aws_iam_role.task-execution-role.arn
  network_mode          = "bridge"
}

resource "aws_cloudwatch_log_group" "service-log" {
  name              = "/${var.environment}/${var.service_name}"
  retention_in_days = "7"

  tags = {
    Environment = var.environment
  }
}

resource "aws_ecs_service" "service" {
  name            = "${var.service_name}-service"
  cluster         = aws_ecs_cluster.assignment-cluster.id
  task_definition = aws_ecs_task_definition.task-definition.arn
  desired_count   = var.desired_task_count

  load_balancer {
    target_group_arn = aws_lb_target_group.target-group.arn
    container_name   = var.service_name
    container_port   = 8010
  }

  lifecycle { 
    ignore_changes = [
      task_definition,
      desired_count
    ]
  }
  depends_on = [aws_lb.vni-service-loadbalancer]
}

### creates a self referencing sg
resource "aws_security_group" "assignment-memberonly-access-sg" {
  name        = "assignment-memberonly-access-sg"
  description = "Allow all members between subnets"
  vpc_id      = module.assignment.vpc_id

  tags = {
    Name        = "assignment-memberonly-access-sg"
    Environment = "assignment"
  }
}

### adds a sg rule that makes the rules for the above sg - assignment-memberonly-access-sg
resource "aws_security_group_rule" "assignment-memberonly-ingress-rule" {
  type                     = "ingress"
  from_port                = 0
  to_port                  = 0
  protocol                 = "-1"
  source_security_group_id = aws_security_group.assignment-memberonly-access-sg.id
  security_group_id        = aws_security_group.assignment-memberonly-access-sg.id
}

resource "aws_security_group_rule" "assignment-loadbancer-http-ingress-rule" {
  type                     = "ingress"
  from_port                = 0
  to_port                  = 0
  protocol                 = "-1"
  source_security_group_id = aws_security_group.vni-loadbalancer-sg.id
  security_group_id        = aws_security_group.assignment-memberonly-access-sg.id
}

resource "aws_security_group_rule" "assignment-memberonly-egress-rule" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.assignment-memberonly-access-sg.id
}

##########################################
##### ECS Load balancing
##########################################

resource "aws_security_group" "vni-loadbalancer-sg" {
  name        = "vni-loadbalancer-sg"
  description = "VNI backend service sg"
  vpc_id      = module.assignment.vpc_id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    description = "allow all"
  }

  tags = {
    Name        = "vni-loadbalancer-sg"
    Environment = "assignment"
  }
}

resource "aws_security_group_rule" "assignment-loadbalancer-http-ingress-rule" {
  type                     = "ingress"
  from_port                = 80
  to_port                  = 80
  protocol                 = "tcp"
  cidr_blocks              = ["0.0.0.0/0"]
  security_group_id        = aws_security_group.vni-loadbalancer-sg.id
}

resource "aws_security_group_rule" "assignment-loadbalancer-https-ingress-rule" {
  type                     = "ingress"
  from_port                = 443
  to_port                  = 443
  protocol                 = "tcp"
  cidr_blocks              = ["0.0.0.0/0"]
  security_group_id        = aws_security_group.vni-loadbalancer-sg.id
}

resource "aws_lb" "vni-service-loadbalancer" {
  name               = "vni-service-loadbalancer"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.vni-loadbalancer-sg.id]
  subnets            = [module.assignment.pub-subnet-a, module.assignment.pub-subnet-b, module.assignment.pub-subnet-c]

  enable_deletion_protection = false

  tags = {
    Environment = "assignment"
  }
}

resource "aws_lb_target_group" "target-group" {
  name     = "${var.service_name}-${var.environment}-target-gp"
  port     = 8010
  protocol = "HTTP"
  vpc_id   = module.assignment.vpc_id

  health_check {
    interval            = 30
    path                = "/healthcheck"
    port                = 8010
    protocol            = "HTTP"
    timeout             = 5
    healthy_threshold   = 3
    unhealthy_threshold = 3
    matcher             = "200"     
  }

  tags = {
    Name        = "${var.service_name}-tg"
    Environment = var.environment
  }
}

resource "aws_lb_listener" "vni-service-loadbalancer-listener" {
  load_balancer_arn = aws_lb.vni-service-loadbalancer.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.target-group.arn
  }
}

resource "aws_lb_listener_rule" "listener-rule" {
  listener_arn = aws_lb_listener.vni-service-loadbalancer-listener.arn
  priority     = var.loadbalancer_listener_rule_priority

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.target-group.arn
  }

  condition {
    field  = "host-header"
    values = [var.service_host_name]
  }
}

##########################################
##### ECS Autoscaling
##########################################

resource "aws_appautoscaling_target" "service-autoscaling-target" {
  max_capacity       = var.max_containers
  min_capacity       = var.min_containers
  resource_id        = "service/${var.environment}-cluster/${var.service_name}-service"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"
  depends_on = [aws_ecs_service.service]
}

resource "aws_appautoscaling_policy" "service-scaledown-policy" {
  name                    = "${var.environment}-${var.service_name}-scale-down"
  policy_type             = "StepScaling"
  resource_id             = "service/${var.environment}-cluster/${var.service_name}-service"
  scalable_dimension      = "ecs:service:DesiredCount"
  service_namespace       = "ecs"

  step_scaling_policy_configuration {
    adjustment_type         = "ChangeInCapacity"
    cooldown                = 60
    metric_aggregation_type = "Maximum"

    step_adjustment {
      metric_interval_upper_bound = 0
      scaling_adjustment          = var.scaledown_step
    }
  }

  depends_on = [aws_appautoscaling_target.service-autoscaling-target]
}

resource "aws_appautoscaling_policy" "service-scaleup-policy" {
  name                    = "${var.environment}-${var.service_name}-service-scaleup-policy"
  policy_type             = "StepScaling"
  resource_id             = "service/${var.environment}-cluster/${var.service_name}-service"
  scalable_dimension      = "ecs:service:DesiredCount"
  service_namespace       = "ecs"

  step_scaling_policy_configuration {
    adjustment_type         = "ChangeInCapacity"
    cooldown                = 60
    metric_aggregation_type = "Average"

    step_adjustment {
      metric_interval_lower_bound = 0
      scaling_adjustment          = var.scaleup_step
    }
  }

  depends_on = [aws_appautoscaling_target.service-autoscaling-target]
}

resource "aws_cloudwatch_metric_alarm" "service-cpu-utilization-above-pct" {
  alarm_name                = "${var.environment}-${var.service_name}-service-cpu-utilization-above-${var.cpu_high_alarm_percent}-pct"
  comparison_operator       = "GreaterThanThreshold"
  evaluation_periods        = "2"
  metric_name               = "CPUUtilization"
  namespace                 = "AWS/ECS"
  period                    = "300"
  statistic                 = "Average"
  threshold                 = var.cpu_high_alarm_percent
  alarm_description         = "${var.environment}-${var.service_name} service cpu utilization above ${var.cpu_high_alarm_percent}%"
  datapoints_to_alarm       = "2"
  insufficient_data_actions = []

  alarm_actions = [
    aws_appautoscaling_policy.service-scaleup-policy.arn
  ]
  
  dimensions = {
    ClusterName = "${var.environment}-cluster",
    ServiceName = "${var.service_name}-service"
  }
  depends_on = [aws_ecs_service.service]
}

resource "aws_cloudwatch_metric_alarm" "service-cpu-utilization-below-pct" {
  alarm_name                = "${var.environment}-${var.service_name}-service-cpu-utilization-below-${var.cpu_low_alarm_percent}-pct"
  comparison_operator       = "LessThanThreshold"
  evaluation_periods        = "2"
  metric_name               = "CPUUtilization"
  namespace                 = "AWS/ECS"
  period                    = "300"
  statistic                 = "Average"
  threshold                 = var.cpu_low_alarm_percent
  alarm_description         = "${var.environment}-${var.service_name} service cpu utilization below ${var.cpu_low_alarm_percent}%"
  datapoints_to_alarm       = "2"
  insufficient_data_actions = []

  alarm_actions = [    
    aws_appautoscaling_policy.service-scaledown-policy.arn
  ]

  dimensions = {
    ClusterName = "${var.environment}-cluster",
    ServiceName = "${var.service_name}-service"
  }
  depends_on = [aws_ecs_service.service]
}

resource "aws_cloudwatch_metric_alarm" "service-memory-utilization-above-pct" {
  alarm_name                = "${var.environment}-${var.service_name}-service-memory-utilization-above-${var.mem_high_alarm_percent}-pct"
  comparison_operator       = "GreaterThanThreshold"
  evaluation_periods        = "2"
  metric_name               = "MemoryUtilization"
  namespace                 = "AWS/ECS"
  period                    = "300"
  statistic                 = "Average"
  threshold                 = var.mem_high_alarm_percent
  alarm_description         = "${var.environment}-${var.service_name}-service memory utilization above ${var.mem_high_alarm_percent}%"
  datapoints_to_alarm       = "2"
  insufficient_data_actions = []

  alarm_actions = [aws_appautoscaling_policy.service-scaleup-policy.arn]

  dimensions = {
    ClusterName = "${var.environment}-cluster",
    ServiceName = "${var.service_name}-service"
  }
  depends_on = [aws_ecs_service.service]
}

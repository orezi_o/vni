variable "service_name" {
    default = "vni-backend"
    description = "ECS service name"
}

variable "desired_task_count" {
    default = 3
    description = "default number of running autoscaling tasks"
}

#ideally would use an m4.large or m4.xlarge as those are memory intensive.
variable "instance_type" {
    default = "t2.small"
    description = "Intsnace type for launch configuration"
}

variable "asg_max_size" {
    default = 10
    description = "maximum number of instances in autoscaling group "
}

variable "asg_min_size" {
    default = 3
    description = "minimum number of instances in group"
}

variable "asg_desired_capacity" {
    default = 3
    description = "desired capacity of autoscaling group"
}

variable "loadbalancer_listener_rule_priority" {
    description = "number has to be unique within load balancer rules"
    default = 55
}

variable "service_host_name" {
    description = "servie host name variable"
    default = "vni-backend.com"
}

variable "environment" {
    description = "environment ex. dev, qa, stage, assignment"
    default = "assignment"
}

variable "min_containers" {
    description = "min amount of containers desired"
    default = 3
}

variable "max_containers" {
    description = "max amount of containers desired"
    default = 10
}

variable "instance_volume_size" {
    description = "The volume size to be associated with container instances"
    default = 20
}

variable "task_memory_limit" {
    description = "The memory limit assigned to the running tasks"
    default = 768
}

variable "scaleup_step" {
    description = "num of containers added in scaleup"
    default = 2
}

variable "scaledown_step" {
    description = "num of containers removed in scaledown"
    default = -1
}

variable "cpu_high_alarm_percent" {
    description = "percent of cpu in high alarm"
    default = 75
}

variable "cpu_low_alarm_percent" {
    description = "percent of cpu in low alarm"
    default = 5
}

variable "mem_high_alarm_percent" {
    description = "percent of mem in high alarm"
    default = 75
}

variable "iam_policy_arn" {
    description = "list of AWS managed policy to attach to sysops group"
    type    = list(string)
    default = [
      "arn:aws:iam::aws:policy/AmazonEC2FullAccess",
      "arn:aws:iam::aws:policy/AmazonECS_FullAccess"
    ]
}

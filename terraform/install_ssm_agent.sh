#! /bin/bash
echo ECS_CLUSTER=assignment-cluster >> /etc/ecs/ecs.config
sudo yum update
sudo yum install -y https://s3.eu-central-1.amazonaws.com/amazon-ssm-eu-central-1/latest/linux_amd64/amazon-ssm-agent.rpm
sudo start amazon-ssm-agent

terraform {
  backend "remote" {
    hostname = "app.terraform.io"
    organization = "orezi"

    workspaces {
      name = "vni"
    }
  }
}
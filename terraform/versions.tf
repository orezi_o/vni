terraform {
  required_version = "= 0.12.20"
  required_providers {
    aws = "= 2.70.0"
  }
}

provider "aws" {
  region  = "eu-central-1"
}

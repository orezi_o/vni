resource "aws_iam_group" "developer" {
  name = "Developer"
}

resource "aws_iam_policy" "developer-cloudwatch-log" {
  name        = "Read-only-Cloudwatch"
  description = "This policy allows members read and descript content in Cloudwatch"
  policy      = file("policies/cloudwatch-read-only.json")
}

resource "aws_iam_policy" "basic-user-policy" {
  name    = "base-user-policy"
  description = "Grants basic access to users"
  policy      = file("policies/base-user.json")
}

resource "aws_iam_group_policy_attachment" "attach-developer-to-policy" {
  group      = aws_iam_group.developer.name
  policy_arn = aws_iam_policy.developer-cloudwatch-log.arn
}

resource "aws_iam_group_policy_attachment" "attach-developer-to-base-policy" {
  group      = aws_iam_group.developer.name
  policy_arn = aws_iam_policy.basic-user-policy.arn
}

resource "aws_iam_group" "sysops" {
  name = "Sysops"
}

resource "aws_iam_policy" "sysops-custom-policy" {
  name        = "sysops-custom-policy"
  description = "Grants custom permissions to members of Sysops group"
  policy      = file("policies/sysops.json")
}

resource "aws_iam_group_policy_attachment" "attach-developer-policy-to-sysops-group" {
  group      = aws_iam_group.sysops.name
  policy_arn = aws_iam_policy.developer-cloudwatch-log.arn
}

resource "aws_iam_group_policy_attachment" "attach-base-policy-to-sysops-group" {
  group      = aws_iam_group.sysops.name
  policy_arn = aws_iam_policy.basic-user-policy.arn
}

resource "aws_iam_group_policy_attachment" "attach-custom-policy-to-sysops-group" {
  group      = aws_iam_group.sysops.name
  policy_arn = aws_iam_policy.sysops-custom-policy.arn
}

resource "aws_iam_group_policy_attachment" "attach-aws-managed-policy-to-sysops-group" {
  group      = aws_iam_group.sysops.name
  count = length(var.iam_policy_arn)
  policy_arn = var.iam_policy_arn[count.index]
}

resource "aws_iam_group" "devops" {
  name = "devops"
}

resource "aws_iam_group_policy_attachment" "attach-admin-policy-to-devops-group" {
  group      = aws_iam_group.devops.name
  policy_arn = "arn:aws:iam::aws:policy/AdministratorAccess"
}

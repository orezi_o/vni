# IAM SETUP

IAM policies, groups and roles are used to manage authorization and authentication to the AWS environment which hosts the VNI backend.

## Configuration
The configuration defined in iam.tf defines the groups and policies attached to those groups. Subsequently, new users can be added to either of these groups to immediately have permissions that are granted to the policies attached to the group.

The table below shows the breakdown the groups and the policies

| Group name | Policies attached         | Permissions                        |  
| ------     |       ------              |    ------                          |  
| Developer  |basic-user-policy          |  Read only other users in IAM      |
|            |developer-cloudwatch-log   |  Edit personal IAM profile         |
|            |                           |  Read only for IAM Cloudwatch logs |
| Sysops     |All developer policies     |  All Developer Permissions         | 
|            |sysops-custom-policy      |  Systems Session Manager to access Instances with tag Key=Name, Value=assignment-ecs-instance|            
| DevOps     |  AdministratorAccess      |  AWS Admin to access all services except billing|

### Test accounts
3 Test user accounts have been created and added to the above groups in AWS:

* Developer
* Sysops
* Devops

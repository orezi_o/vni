const WebSocket = require("ws");
const name = "VNI-Backend"
const server = require("http").createServer();
const express = require("express");
const app = express();

console.log("This server is named: ", name);

// serve files from the public directory
server.on("request", app.use(express.static("public")));

//home route
app.get('/', function(req, res) {
  res.sendFile(appDir + '/public/index.html');
});

app.get('/healthcheck', (req, res) => {
  var used = process.memoryUsage().heapUsed / 1024 / 1024;
  used = Math.round(used * 100) / 100;
  console.log('The server is using: ', used + 'MB');
  const serverHasCapacity = used < 3200
  if (serverHasCapacity) res.status(200).send("ok");
  res.status(400).send("server is overloaded");
});

// tell the WebSocket server to use the same HTTP server
const wss = new WebSocket.Server({
  server,
});

wss.on("connection", function connection(ws, req) {
  
  console.log("Client connected with VNI backend");

  ws.on('message', function(message) {
    console.log('Received from client: %s', message);
    ws.send('Server received from client: ' + message);
  });
  
  let n = 0;
  const interval = setInterval(() => {
    ws.send("you have been connected for: " + n++ + " seconds");
  }, 1000);

  ws.on("close", () => {
    clearInterval(interval);
  });
});

const port = 8010;
server.listen(port, () => {
  console.log("Server listening on port ${port}");
});
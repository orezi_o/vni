# VNI Infrastructure

An infrastructure built with terraform, container a backend websocket server running on ECS containers.

## Description

### Environment Design
The architecture design utilizes AWS Elastic container service to host and deploy the VNI Backend on docker. The image below describes the components of the architecture that work together to create a highly available and reliable infrastructure with ECS. This solution was chosen over the one in [Architecture-1-codedeploy.png](https://bitbucket.org/orezi_o/vni/src/master/Architecture-1-codedeploy.png) because:
* ECS is by nature built to handle deployments in docker. It provides proper autodeploys on revision/image updates, autoscaling and loadbalancing of tasks without downtime.
* Using Terraform cloud manages the infrastructure and gives a continuous deployment pattern. It autodetects changes in the source repositories and performs a build to check for errors in configuration before running a `terraform plan` and `terraform apply`.

![Design](Architecture-2-ecs.png)

#### Components

##### Bitbucket
The seemless plug and play with Atlassian products influenced the decision to use bitbucket as the git repository. Automatic builds and runs can be triggered on pull requests. This allows for quick feedback

##### VNI Backend
The backend folder holds the code for the VNI Backend. It is simple node.js script which runs a websocket on port 8010 and also serves a basic HTML file on request. It also accepts GET requests on `/healtcheck` which checks the memory usage and reports "ok" if it less than 3000mb used on that instance.
The VNI backend has been dockerized and pushed to docker hub public repo as an image named: `orezi/newmotion-vni-backend`. This image is later pulled in by ECS task definition and deployed.

#### Output and Testing
The loadbalancer endpoint handles incoming requests on the following DNS:

* vni-service-loadbalancer-321741422.eu-central-1.elb.amazonaws.com
* vni-service-loadbalancer-321741422.eu-central-1.elb.amazonaws.com/healthcheck

To test a connection to the web socket - you can the `wscat` node package:
* npm install wscat -g
* wscat --connect ws://vni-service-loadbalancer-321741422.eu-central-1.elb.amazonaws.com

##### Terraform and Terraform cloud
Terraform is used as Infrastructure as code. This is because it is cloud agnostic, this makes it easy to be ported to Google Cloud compute or Azure if we wanted. It allows for flexibility in design.

The terraform folder holds the infrastructure configuration. The configuration consists of one module (service/vpc.tf) and two files: `ecs.tf` and `iam.tf`

###### [vpc.tf](https://bitbucket.org/orezi_o/vni/src/master/terraform/service/vpc.tf)
A custom VPC was created in the Frankfurt region in AWS. This is for separation of concern and to be able to control what type of connections are accepted into the AWS resources. Resources created:

* vpc
* internet gateway
* 3 public subnets in separate availablity zones for high availability
* 3 route tables and associates routing traffic through the internet gateway

###### [ecs.tf](https://bitbucket.org/orezi_o/vni/src/master/terraform/ecs.tf)
This file holds the configuration for the whole ECS cluster and the resources needed to autoscale in/out and loadbalance.
The resources created:

* Ecs cluster
* Ecs service with 2 tasks as minimum
* Task definition with 768Mib provisioned. It uses Network bridge to map the container port 8010 to host port 8010
* Autoscaling group with 2 minimum and 10 maximum instances to start with.
* Launch configuration using latest Amazon Linux AMI and 100gb of memory by default. It also installs SSM Agent using the user data script. It launches instances across the 3 availability zones from the public subnets
* Target group using HTTP healtchecks to ping the `/healthcheck` path
* Listener rules to listen on port 80 on the loadbalancer and forward to the target group
* Application Loadbalancer forwarding requests to the target group
* Security Groups of the Autoscaling group - Only allows inbound connections from the loadbalancer and from resources in the VPC subnets (members only)
* Security group of the loadbalancer - Only accepts incoming connections on port 80 and 443 and
* IAM roles to enable task execution and autoscaling of the services for ECS
* Scale up and Scale down policies
* Cloudwatch Alarms for CPU Usage that notify the scale up policy when usage is 75% and scales down when it is 25%
* Cloudwatch Alarms for Memory utilization that notify the scale up policy when usage is 75% and scales down when it is 25%
* Cloudwatch Log group to send the application logs for debugging and insight using amazon-ecs-agent

###### [iam.tf](https://bitbucket.org/orezi_o/vni/src/master/terraform/iam.tf)
This manages the groups and policies for the AWS Account. Authenticaion and Authorization, See [authorization/README.md](https://bitbucket.org/orezi_o/vni/src/master/terraform/authorization).

###### [variables.tf](https://bitbucket.org/orezi_o/vni/src/master/terraform/variables.tf)
This holds the default value for the variables used in the terraform configuration files. Edit the value here to change the parameters terraform uses to deploy the resources. 

###### Applying changes

* Clone this repository
* Run `terraform init` to initialize the backend
* Make your changes
* Run `terraform plan` to see what will be changed if an apply is ran
* Git commit and push to the `dev` branch in bitbucket
* Create a pull request
* After the build passes and the pull request has been approved, a plan will be queued in terraform cloud
* The plan has to be confirmed and applied manually, for the changes to be deployed to AWS

#### Debugging your application in ECS
In case of errors or issues with the version of the app deployed. Developers need to be able to debug the application without having to connect to the servers manually. To enable them do this, a log group has been created alongside the ECS service using the "<environment>/<service-name>" variables. It creates log streams as the application is being used. Any in AWS who has been assigned to the `Developer` group has been granted access rights to Read Cloudwatch Alarms and Groups. Developers can visit cloudwatch to see errors coming from the application.

![Design](cloudwatch-log-stream.png)

Backend instances can also be reached using Systems Session Manager. It gives a secure shell in the AWS Console or AWS CLI to the authorized users only. For this, a SysOps group was created which gives members elevated priviledges to connect to the instances and debug the application directly by running commands.
